<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/KCORP/footer.html.twig */
class __TwigTemplate_66fc024c10ef8c81f360c6f42732a655d6134b1f1f7a3b001f5e8d3d2fd9ac5d extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = [];
        $filters = ["escape" => 17];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                [],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "

  <footer>
       
        <div class=\"footer\">
            <div class=\"blue-background\">
            <div class=\"container\">
            <div class=\"row\">
               
                <div class=\"col-md-3 col-sm-12 col-xs-12\">
                   <div class=\"map\">
                       <iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3383.8151125847858!2d35.870220814515335!3d31.993027130852372!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x151ca102cbe2787f%3A0xbdd6f87e40fa5da2!2sFikra%20for%20business%20development!5e0!3m2!1sen!2sjo!4v1572410446821!5m2!1sen!2sjo\" width=\"200\" height=\"150\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>
                   </div>
                </div>
                <div class=\"col-md-3 col-sm-3 col-xs-12\">
                    <div class=\"footer-contact\">
                  ";
        // line 17
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "about_us", [])), "html", null, true);
        echo "
                    </div>
                    
                </div>

                <div class=\"col-md-3 col-sm-12 col-xs-12\">
                    <div class=\"footer-links\">
                 ";
        // line 24
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_links", [])), "html", null, true);
        echo "
                    </div>
                </div>

                <div class=\"col-md-3 col-sm-12 col-xs-12\">
                
                    <div class=\"contact\">
                      ";
        // line 31
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_contact", [])), "html", null, true);
        echo "
                        <div class=\"search\">
                                    <input type=\"text\" class=\"form-control\" placeholder=\"Search entier Store here\">
                                    <button class=\"search-btn\">
                                        <i class=\"fa fa-search\" style=\"font-size:40px\"></i>
                                    </button>
                                </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </footer>";
    }

    public function getTemplateName()
    {
        return "themes/KCORP/footer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 31,  83 => 24,  73 => 17,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/KCORP/footer.html.twig", "C:\\wamp64\\www\\KCORP\\themes\\KCORP\\footer.html.twig");
    }
}
