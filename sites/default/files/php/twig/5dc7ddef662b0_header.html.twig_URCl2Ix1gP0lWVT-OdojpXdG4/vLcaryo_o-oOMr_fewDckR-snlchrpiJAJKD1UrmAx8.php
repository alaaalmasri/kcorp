<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/KCORP/header.html.twig */
class __TwigTemplate_3bda2c26481c85a4fa4d885bf5a6bdeebb18eda68f32b0b45aabc59a21a3fb91 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = [];
        $filters = ["escape" => 9];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                [],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "

<body>
    <div class=\"header-background\">
        <div class=\"header\">
            <div class=\"row\">
                <div class=\"col-md-2 col-sm-12 col-xs-12\">
                    <div class=\"contact-info\">
                       ";
        // line 9
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "contact", [])), "html", null, true);
        echo "
                    </div>
                    <!--end of contact information-->
                </div>
                <div class=\"col-md-8 col-sm-12 col-xs-12\">
                    <div class=\"image-logo\">
                        <center>
                            <img src=\"themes/KCORP/assets/image/Logo.png\" class=\"image-responsive\">
                        </center>
                    </div><!-- end of logo -->
                </div>
                <!--end of col-->
                <div class=\"col-md-2 col-sm-12 col-xs-12\">
                    <div class=\"social-media\">
                      
                   ";
        // line 24
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "social_icons", [])), "html", null, true);
        echo "
                    </div><!-- end of social-media-->
                </div>
                <!--end of col-->
            </div><!-- end of row-->
            <div class=\"main-nav\">
                <nav class=\"navbar navbar-expand-lg navbar-light\">
                    <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
                        <span class=\"navbar-toggler-icon\"></span>
                    </button>

                    <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
                        <ul class=\"navbar-nav mr-auto\">
                         ";
        // line 37
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "main_navbar", [])), "html", null, true);
        echo "
                            <li>
                                <div class=\"search\">
                                    <input type=\"text\" class=\"form-control\" placeholder=\"Search entier Store here\">
                                    <button class=\"search-btn\">
                                        <i class=\"fa fa-search\" style=\"font-size:40px\"></i>
                                    </button>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <!--end of header-->
    </div>
    <!--end of header-background-->";
    }

    public function getTemplateName()
    {
        return "themes/KCORP/header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 37,  83 => 24,  65 => 9,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/KCORP/header.html.twig", "C:\\wamp64\\www\\KCORP\\themes\\KCORP\\header.html.twig");
    }
}
