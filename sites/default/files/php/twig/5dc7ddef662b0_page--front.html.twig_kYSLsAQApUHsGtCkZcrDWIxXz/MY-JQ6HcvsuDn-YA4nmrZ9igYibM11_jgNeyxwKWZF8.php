<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/KCORP/page--front.html.twig */
class __TwigTemplate_7945d5d32feed1c9fe31795bf09c998ea88a084e0dee4d33a38678efa49b0671 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["include" => 1];
        $filters = ["escape" => 4];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['include'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $this->loadTemplate((($context["directory"] ?? null) . "/header.html.twig"), "themes/KCORP/page--front.html.twig", 1)->display($context);
        // line 2
        echo "
    <section>
    ";
        // line 4
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "slider", [])), "html", null, true);
        echo "
    </section>
    <section>
        <div class=\"Kcrop-introduction\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-md-6 col-sm-12 col-xs-12\">
                        <div class=\"Kcrop-content\">
                               ";
        // line 12
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "kcoprp", [])), "html", null, true);
        echo "
                        </div>
                        <div class=\"know-more-btn\">
                            <button class=\"btn btn-primary\">KNOW MORE</button>
                        </div>
                    </div>
                    <!--END OF COL-->
                    <div class=\"col-md-6 col-sm-12 col-xs-12\">
                        <div class=\"Kcrop-introdution-image\">
                            <img src=\"themes/KCORP/assets/image/WELCOME-KCorp.png\" class=\"img-responsive\">
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!--end of crop introduction-->
    </section>
    <!--end of crop-introduction section-->
    <section>
        <div class=\"kcrop-services\">
            ";
        // line 33
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "qualification_title", [])), "html", null, true);
        echo "
            <div class=\"services-section\">
                <div class=\"container\">
                ";
        // line 36
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "kcorp_qualfication", [])), "html", null, true);
        echo "
            </div>
        </div>
        </div>
    </section>
    <!--end of KCORP INTRODUCTION-->
    <section>
        <div class=\"kcorp-products\">
            <div class=\"kcrop-products-header\">
              ";
        // line 45
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "service_products_title", [])), "html", null, true);
        echo "
                
            </div>

            
                ";
        // line 50
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "service", [])), "html", null, true);
        echo "
            </div>
    
    </section>
    <!--end of kcorp product section-->
    <section>
        <div class=\"kcorp-aniversary\">
        </div>
    </section>
    <section>
        <div class=\"contact-form\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-md-6 cols-sm-6 col-xs-12\">
                        <div class=\"text\">
                            <h2>DO YOU HAVE ANY Questions?</h2>
                            <h2>FEEL FREE TO CONTACT US</h2>
                        </div>
                    </div>
                    <div class=\"col-md-6 col-sm-6 col-xs-12\">
                        <div class=\"form-row\">
                            <div class=\"form-group col-md-6\">
                                <input type=\"text\" class=\"form-control\" placeholder=\"name\">
                            </div>
                            <div class=\"form-group col-md-6\">
                                <input type=\"email\" class=\"form-control\" placeholder=\"email\">
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <input type=\"text\" class=\"form-control\" id=\"message\" placeholder=\"message\">
                            <button type=\"submit\" class=\"btns mb-2\" style=\"padding: 10px;\">Submit</button>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end of contact form-->
    <section>
        <div class=\"container\">
            <div class=\"sector-service\">
                <div class=\"sector-service-header\">
                    ";
        // line 94
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "service_sector", [])), "html", null, true);
        echo "
                    <center>
                        <button class=\"btn btn-primary\">READ MORE</button>
                    </center>
                </div>
            </div>
                <div class=\"industrial\">
          ";
        // line 101
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "service_sector_items", [])), "html", null, true);
        echo "
                </div>
        </div>
    </section>
    <!--end of of industrial sector-->
    <section>
        <div class=\"counter\">
            <div class=\"container\">
                ";
        // line 109
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "counter", [])), "html", null, true);
        echo "
               
            </div>
        </div>
    </section>
    <!--end of counter-->
    <section>
        <div class=\"customer-reviews\">
            <div class=\"container\">
                <div class=\"customer-reviews-header\">
                  ";
        // line 119
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "customer_review", [])), "html", null, true);
        echo "
                </div>

                <div class=\"customer-reviews-items\">
                   ";
        // line 123
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "item", [])), "html", null, true);
        echo "
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class=\"service\">
            <img src=\"themes/KCORP/assets/image/WE-OFFER.png\" class=\"img-responsive\">
            <div class=\"service-texts\">
                <h2>WE OFFER AN EXCELENT AFTER SERVICE</h2>
            </div>
            <div class=\"service-btn\">
                <button class=\"btn btn-primary\">OUR SERVICE</button>
                <button class=\"btn btn-primary\">CONTACT US</button>
            </div>
        </div>
    </section>
    <!--end of service-->
    <div class=\"news-events\">
            <div class=\"row\">
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    <div class=\"news-events-section\">
                        <h2>NEWS &amp; EVENTS</h2>
                        <div class=\"event-images\">
                                    <div class=\"event-image\">
                                        ";
        // line 148
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "newsandevents", [])), "html", null, true);
        echo "

                                    </div>
                        </div>

                        </div>
                    </div>
                       <div class=\"col-md-6 col-sm-6 col-xs-12\">
                            ";
        // line 156
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "newsandeventsitems", [])), "html", null, true);
        echo "
                </div>
                
             
            </div>
    </div>
       ";
        // line 162
        $this->loadTemplate((($context["directory"] ?? null) . "/footer.html.twig"), "themes/KCORP/page--front.html.twig", 162)->display($context);
        // line 163
        echo "
    <div class=\"copyrght\">
        <p>Copright@Fikra All rights reserved</p>
    </div>
    <script type=\"text/javascript\">
        \$(\".num\").counterUp({
            delay: 10,
            time: 3000
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "themes/KCORP/page--front.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  260 => 163,  258 => 162,  249 => 156,  238 => 148,  210 => 123,  203 => 119,  190 => 109,  179 => 101,  169 => 94,  122 => 50,  114 => 45,  102 => 36,  96 => 33,  72 => 12,  61 => 4,  57 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/KCORP/page--front.html.twig", "C:\\wamp64\\www\\KCORP\\themes\\KCORP\\page--front.html.twig");
    }
}
